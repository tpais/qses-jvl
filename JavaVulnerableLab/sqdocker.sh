#! /bin/bash

st=$(docker ps -a -f name=sonarqube --format {{.Status}})

case "$st" in 
	Up* ) 	echo SQ container already active 
		;; 
	Exit* ) echo SQ container restarting ...
 		docker start -a sonarqube 
		;;
  	* ) 	echo SQ container initializing and running for the first time ...
		docker run --name sonarqube -p 9000:9000 sonarqube 
		;;
esac


