# Java Vulnerable Lab
## Prerequisites
**Install docker**
(https://docs.docker.com/v17.09/engine/installation/)

## Description
This Project starts JavaVulnerableLab application in conjunction with a Static Analysis report performed by SonarQube. The project needs some minutes to build, run scans, and run the application before the application be ready to use.

## How to run
```bash
$ docker-compose up
```

The service will be available on https://localhost:9443/

## SonarQube Reports
After project start wait some minutes until SonarQube report be available on http://localhost:9000/projects

## Support Documentation
https://www.dcc.fc.up.pt/~edrdo/aulas/qses/projects/01/ferramentas.html
