FROM maven:3.6-jdk-8

CMD cd /JVL &&\
    mvn clean package &&\
    mvn package sonar:sonar &&\
    mvn package exec:exec
